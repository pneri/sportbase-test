# SportBase-Test

## Résumé

Ce script à pour but de créer un dump d'une base de donnée connue soit sélectionner toutes les datas, et de les copier vers une seconde base de données sur une serveur différent ou non tout en ajoutant un préfixe sur un ou plusieurs attributs d'une ou plusieurs tables pour pouvoir faire la différence entre ces 2 BDD.


Exemple :

### Avant dump

Base de données source :
```
---
select nom,prenom from candidat;
---
```
![Résultat SELECT base source](assets/images/screen_exemple_base1.png)

Base de données de destination :
```
---
select nom,prenom from candidat;
---
```
![Résultat SELECT base destination avant dump](assets/images/screen_exemple_base2.png)

### Après dump

Base de données de destination :
```
---
select nom,prenom from candidat;
---
```
![Résultat SELECT base destination après dump](assets/images/screen_exemple_base3.png)

## Utilisation 

Il y a 2 fichier obligatoire à avoir pour le fonctionnement du script :
 - script.sh
 - config.yml

script.sh correspond au programme à lancer pour dump, et le fichier config.yml contient les informations dont le script à besoin pour fonctionner :

![Résultat SELECT base destination après dump](assets/images/screen_exemple_config.png)

Ici on retrouve l'utilisateur pour les 2 bases, l'hébergement, le port et les mots de passe. On y trouve également la ou les tables ciblées pour le préfixage (séparées par une virgule), la ou les colonnes (séparées par une virgule) et enfin le préfixe.

Si on veut modifier les paramètres de connexion ou de modification, il faut modifier ce fichier.


Une fois les paramètres entrés, il suffit de taper cette ligne de commande dans un terminal dans le dossier courant des 2 fichiers :
```
---
./script.sh
---
```



