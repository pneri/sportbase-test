#!/bin/bash

parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

# read yaml file
eval $(parse_yaml config.yml "config_")

configHead=$(head -1 config.yml)

if [ $configHead == "dump:" ]; then

   echo $configHead
   rm -f -- dump_schema_base_src.sql
   rm -f -- dump_schema_base_dest.sql

   PGPASSWORD="$config_dump_BDDorigine_password" pg_dump -U $config_dump_BDDorigine_user \
   -h $config_dump_BDDorigine_host -p $config_dump_BDDorigine_port $config_dump_BDDorigine_databaseName -s -O -x >> dump_schema_base_src.sql

   PGPASSWORD="$config_dump_BDDdestination_password" pg_dump -U $config_dump_BDDdestination_user -h \
   $config_dump_BDDdestination_host -p $config_dump_BDDdestination_port $config_dump_BDDdestination_databaseName -s -O -x >> dump_schema_base_dest.sql

   if [[ $(md5sum dump_schema_base_src.sql dump_schema_base_dest.sql | awk '{print $1}' | uniq | wc -l) == 1 ]]
   then
      echo "Les schémas sont biens identiques"

         echo "CREATE OR REPLACE FUNCTION truncate_tables(username IN VARCHAR) RETURNS void AS \$$
      DECLARE
         statements CURSOR FOR
            SELECT tablename FROM pg_tables
            WHERE tableowner = username AND schemaname = 'public';
      BEGIN
         FOR stmt IN statements LOOP
            EXECUTE 'TRUNCATE TABLE ' || quote_ident(stmt.tablename) || ' CASCADE;';
         END LOOP;
      END;
      \$$ LANGUAGE plpgsql;

      SELECT truncate_tables('$config_dump_BDDdestination_user');" >> truncate_tables_dest.sql

      PGPASSWORD="$config_dump_BDDdestination_password" psql -U $config_dump_BDDdestination_user -h \
      $config_dump_BDDdestination_host -p $config_dump_BDDdestination_port $config_dump_BDDdestination_databaseName \
      -a -q -f truncate_tables_dest.sql

      PGPASSWORD="$config_dump_BDDorigine_password" pg_dump -U $config_dump_BDDorigine_user \
      -h $config_dump_BDDorigine_host -p $config_dump_BDDorigine_port $config_dump_BDDorigine_databaseName \
      | PGPASSWORD="$config_dump_BDDdestination_password" psql -U $config_dump_BDDdestination_user -h \
      $config_dump_BDDdestination_host -p $config_dump_BDDdestination_port $config_dump_BDDdestination_databaseName 


      echo "CREATE OR REPLACE FUNCTION add_column_dump(tablename text, columnname text, prefix text) 
      RETURNS void AS 
      \$body$
      DECLARE
         querysql text;
      begin
         EXECUTE format('UPDATE %I SET %I = %L || %I.%I WHERE %I NOT LIKE CAST(%L AS varchar) || CAST(''%%'' AS varchar)', tablename, columnname, prefix,  tablename, columnname, columnname, prefix);
         EXECUTE format('UPDATE candidat SET date_de_naissance = ''1999-01-01'' WHERE DATE_PART(''year'', NOW()) - DATE_PART(''year'', date_de_naissance) >= 18');
         EXECUTE format('UPDATE candidat SET id_rci = CAST(left(CAST(id_rci AS varchar), -4) || ''0000'' AS bigint)');
      end;
      \$body$
      LANGUAGE plpgsql;" >> edit_sql_dump.sql

      IFS=","

      read -a strarr <<< "$config_dump_table"
      read -a str <<< "$config_dump_column"

      for table in "${strarr[@]}";

      do 
         
         for col in "${str[@]}";
         do   
            
            echo "select * from add_column_dump('$table', '$col', '$config_dump_prefix');" >> edit_sql_dump.sql
         done
      done 

      PGPASSWORD="$config_dump_BDDdestination_password" psql -U $config_dump_BDDdestination_user -h \
      $config_dump_BDDdestination_host -p $config_dump_BDDdestination_port $config_dump_BDDdestination_databaseName \
      -a -q -f edit_sql_dump.sql

      rm ./edit_sql_dump.sql
      rm ./truncate_tables_dest.sql

      PGPASSWORD="$config_dump_BDDdestination_password" psql -U $config_dump_BDDdestination_user -h \
      $config_dump_BDDdestination_host -p $config_dump_BDDdestination_port \
      -c 'DROP FUNCTION IF EXISTS add_column_dump;'

      PGPASSWORD="$config_dump_BDDdestination_password" psql -U $config_dump_BDDdestination_user -h \
      $config_dump_BDDdestination_host -p $config_dump_BDDdestination_port \
      -c 'DROP FUNCTION IF EXISTS truncate_tables;'
      
   else
      echo "/!\ ATTENTION : Les bases de données possèdent un schéma différent, vous pouvez consulter et comparer les fichiers dump_schema_base_src.sql et dump_schema_base_dest.sql"  
   fi
elif [ $configHead=="target:" ]; then

   echo "CREATE OR REPLACE FUNCTION add_column_dump(tablename text, columnname text, prefix text) 
      RETURNS void AS 
      \$body$
      DECLARE
         querysql text;

      begin
         EXECUTE format('UPDATE %I SET %I = %L || %I.%I WHERE %I NOT LIKE CAST(%L AS varchar) || CAST(''%%'' AS varchar)', tablename, columnname, prefix,  tablename, columnname, columnname, prefix);
         EXECUTE format('UPDATE candidat SET date_de_naissance = ''1999-01-01'' WHERE DATE_PART(''year'', NOW()) - DATE_PART(''year'', date_de_naissance) >= 18');
         EXECUTE format('UPDATE candidat SET id_rci = CAST(left(CAST(id_rci AS varchar), -4) || ''0000'' AS bigint)');
         
      end;
      \$body$
      LANGUAGE plpgsql;" >> edit_sql_dump.sql

      IFS=","

      read -a strarr <<< "$config_target_table"
      read -a str <<< "$config_target_column"

      for table in "${strarr[@]}";

      do 
         
         for col in "${str[@]}";
         do   
            
            echo "select * from add_column_dump('$table', '$col', '$config_target_prefix');" >> edit_sql_dump.sql
         done
      done 

      PGPASSWORD="$config_target_BDDdestination_password" psql -U $config_target_BDDdestination_user -h \
      $config_target_BDDdestination_host -p $config_target_BDDdestination_port $config_target_BDDdestination_databaseName \
      -a -q -f edit_sql_dump.sql

      rm ./edit_sql_dump.sql

      PGPASSWORD="$config_target_BDDdestination_password" psql -U $config_target_BDDdestination_user -h \
      $config_target_BDDdestination_host -p $config_target_BDDdestination_port \
      -c 'DROP FUNCTION IF EXISTS add_column_dump;'

fi

   






#PGPASSWORD="peactions" pg_dump -U peactions -h localhost -p 5432 peactions | PGPASSWORD="postgres" psql -U postgres -h localhost -p 5431 test_pg

#PGPASSWORD="postgres" psql -U postgres -h localhost -p 5431 test_pg < scriptpsql.sql
